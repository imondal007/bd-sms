import React from 'react'
import Text from 'react-format-text'
import ClipboardJS from 'clipboard'
import Notifications, { notify } from 'react-notify-toast'

class PostCard extends React.Component {
  componentDidMount() {
    new ClipboardJS('.copy-btn')
  }
  handleCopy() {
    notify.show('SMS Copied Successfully', 'error')
  }
  render() {
    const props = this.props
    return (
      <div style={styles.postCard}>
        <span style={styles.category}>#{props.category}</span>
        <p style={styles.text}>
          <Text>{props.text}</Text>
        </p>
        <button
          style={styles.button}
          data-clipboard-text={props.text}
          className='copy-btn'
          onClick={this.handleCopy}
        >
          Copy SMS
        </button>
        <a style={styles.button} href={`sms:?body=${props.text}`}>Send SMS</a>
        <Notifications />
      </div>
    )
  }
}

export default PostCard

const styles = {
  text: {
    marginBottom: 10,
    lineHeight: '1.5em'
  },
  postCard: {
    borderBottom: '2px solid #f0f0f0',
    padding: '10px 0'
  },
  category: {
    display: 'inline-block',
    backgroundColor: '#0055ff',
    padding: 4,
    color: '#ffffff',
    marginBottom: 10
  },
  button: {
    marginRight: 10,
    padding: '8px 15px',
    backgroundColor: '#ffffff',
    border: '1px solid #eee',
    borderRadius: 3,
    textDecoration: 'none',
    color: '#2d4059',
    fontSize: 14,
    fontFamily: 'sans-serif',
    fontWeight: 'bold',
    boxSizing: 'border-box',
    cursor: 'pointer'
  }
}