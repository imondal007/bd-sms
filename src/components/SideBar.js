import React from 'react';
import {Link} from 'react-router-dom';

const SideBar = () => (
  <aside className='sidebar'>
    <ul>
      <li>
        <Link to='/sms/bangla-romantic-love-sms' style={styles.sidebarLink}>
          ভালোবাসার রোম্যান্টিক এস এম এস [Bangla Romantic Love SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-sad-sms' style={styles.sidebarLink}>
          দুঃখের এস এম এস [Bangla Sad SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-good-morning-sms' style={styles.sidebarLink}>
          শুভ সকাল এস এম এস [Good Morning SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-good-night-sms' style={styles.sidebarLink}>
          শুভ রাত্রি এস এম এস [Good Night SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-happy-birthday-sms' style={styles.sidebarLink}>
          শুভ জন্মদিনের এস এম এস [Birthday SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-eid-sms' style={styles.sidebarLink}>
          ঈদের এস এম এস [Bangla Eid SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-durga-puja-sms' style={styles.sidebarLink}>
          দুর্গা পুজার এস এম এস [Durga Puja SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/bangla-nobo-borso-sms' style={styles.sidebarLink}>
          শুভ নববর্ষ এস এম এস [Noboborso SMS]
        </Link>
      </li>
      <li>
        <Link to='/sms/new-year-sms' style={styles.sidebarLink}>
          নিউ ইয়ার এস এম এস [New Year SMS]
        </Link>
      </li>
    </ul>
  </aside>
);

export default SideBar;

const styles = {
  sidebarLink: {
    textDecoration: 'none',
    marginBottom: 15,
    display: 'block'
  }
}
