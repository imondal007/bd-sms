import React, { Component } from 'react'
import Link from 'gatsby-link'

import logo from './logo.png'
import { container } from '../../globalStyle'

class Header extends Component {
  render() {
    return (
      <header style={styles.header}>
        <div style={container}>
          <Link to='/'>
            <img src={logo} style={styles.logo} alt='logo' />
          </Link>
        </div>
      </header>
    )
  }
}

export default Header

const styles = {
  header: {
    backgroundColor: '#feebef',
    padding: '7px 0',
  },
  logo: {
    height: '35px'
  }
}