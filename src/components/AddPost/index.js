import React, { Component } from 'react'
import base, { app } from '../../base'
import firebase from 'firebase/app'
import 'firebase/auth'
import {container, space_between, content_spacing, d_flex, center, a_center} from '../../globalStyle'

class AddPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      category: [
        'Birth Day',
        'Durga Puja',
        'Eid',
        'Love',
        'Good Morning',
        'New Year',
        'Good Night',
        'Nobo Borso',
        'Romantic',
        'Sad'
      ],
      newPost: {
        text: '',
        category: ''
      },
      user: {},
      adminUid: '',
      userDataFetching: false
    }
    this.handleText   = this.handleText.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleAuth   = this.handleAuth.bind(this)
    this.handSelect   = this.handSelect.bind(this)
  }

  componentDidMount() {
    const that = this;
    this.setState({ userDataFetching: true })
    firebase.auth().onAuthStateChanged(function(userinfo) {
      if (userinfo) {
        const user  = { uid: userinfo.uid, name: userinfo.displayName }
        that.setState({ user })
      }
    });
    base.fetch('users', {
      context: this,
      asArray: true,
      then(data){
        const adminUid = data[0].key
        that.setState({ adminUid, userDataFetching: false })
      }
    })
  }

  handleText(e) {
    const text = e.target.value;
    const { newPost } = this.state;
    this.setState({ newPost: { ...newPost, text } });
  }


  handSelect(e) {
    const category = e.target.value;
    const { newPost } = this.state;
    this.setState({ newPost: { ...newPost, category } });
  }

  handleAuth(userexist) {
    const that = this
    const provider = new firebase.auth.GoogleAuthProvider()

    firebase.auth(app).signInWithPopup(provider)
    .then(function() {
      const uid   = firebase.auth().currentUser.uid
      const name  = firebase.auth().currentUser.displayName
      const user  = { uid, name }
      that.setState({ user })
    })

  }

  handleSubmit() {
    const { newPost } = this.state;
    const that = this;
    base.push('posts', {
      data: newPost,
      then(){
        that.setState({
          newPost: {
            text: '',
            category: ''
          }
        })
      }
    })
  }

  render() {
    const { newPost, category, user, adminUid, userDataFetching } = this.state

    if ( userDataFetching ) {
      return (
        <h1>Loading app, please wait</h1>
      )
    }

    if (!user.uid) {
      return(
        <div
        style={{
          ...container,
          ...center,
          ...a_center,
          ...d_flex,
          height: '93vh',
          flexDirection: 'column'
        }}
        >
        <h1 style={styles.title}>Please login to continue</h1>
        <button
        onClick={this.handleAuth}
        style={{...styles.button, ...styles.loginButton}}
        >
        Login
        </button>
        </div>
      )
    }

    if (user.uid !== adminUid) {
      return (
        <h1>User Does not Exist</h1>
      )
    }

    return (
      <div style={{
        ...container,
        ...content_spacing
      }}
      >
      <textarea
      style={styles.textarea}
      placeholder='Add post'
      value={newPost.text}
      onChange={this.handleText}
      />
      <select
      style={styles.category}
      value={this.state.value}
      onChange={this.handSelect}
      >
      <option value='notselected'>Category</option>
      {category &&
        category.map(item => <option value={item} key={item}>{item}</option>)
      }
      </select>
      <div
      style={{
        ...space_between,
        ...d_flex
      }}
      >
      <button
      style={styles.button}
      onClick={this.handleSubmit}
      >
      Add Post
      </button>
      </div>
      </div>
    )
  }
}

export default AddPost

const styles = {
  title: {
    fontSize: 30,
    fontFamily: 'monospace',
    marginBottom: 20
  },
  textarea: {
    display: 'flex',
    width: '100%',
    marginTop: 20,
    minHeight: 250,
    padding: 20,
    boxSizing: 'border-box'
  },
  button: {
    backgroundColor: '#0074e4',
    width: '100%',
    padding: 10,
    textAlign: 'center',
    justifyContent: 'center',
    color: '#ffffff',
    fontWeight: 'bold',
    border: 0,
    cursor: 'pointer',
    fontFamily: 'monospace'
  },
  loginButton: {
    width: '30%'
  },
  category: {
    display: 'flex',
    width: '100%',
    padding: 10,
    margin: '10px 0'
  }
}
