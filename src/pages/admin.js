import React from 'react'
import AddPost from '../components/AddPost'
import Helmet from 'react-helmet'

const Admin = () => (
    <div>
        <Helmet>
          <title>Admin Panel</title>
          <meta name='description' content='BD SMS Admin' />
          <meta name='robots' content='noindex' />
        </Helmet>
        <AddPost />
    </div>
)

export default Admin
