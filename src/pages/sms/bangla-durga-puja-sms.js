import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class DurgaSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const durga = filter(posts, item => item.node.category == 'Durga Puja' )
    const orderByLatest = durga.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Durga Puja SMS 2018 [দূর্গা পুজা এসএমএস]</title>
          <meta name='description' content='Wish your special one in the occesion of saradiyo Durgotsava by sending beautiful sms. Check our list of latest durga puja sms in bangla' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
          <article className='article-body'>
            <header className='article-head'>
              <h1>Bangla Durga Puja SMS 2018<span>দূর্গা পুজা এসএমএস ২০১৮</span></h1>
              <p>Wish your special one in the occesion of saradiyo Durgotsava by sending beautiful sms. Check our list of latest durga puja sms in bangla</p>
            </header>
            {orderByLatest.map(({ node }) => {
              return (
                <PostCard
                    key={node.id}
                    text={node.text}
                    category={node.category}
                    handleCopy={this.handleCopy}
                  />
              )
            })}
          </article>
        <SideBar />
      </div>
    )
  }
}

export default DurgaSMS

export const pageQuery = graphql`
  query DurgaQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
