import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class EidSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const eid = filter(posts, item => item.node.category == 'Eid')
    const orderByLatest = eid.reverse()
  
    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Eid SMS 2018 [ঈদ এসএমএস]</title>
          <meta name='description' content='Eid is all about sharing and caring, let your beloved people know how much you care about them by sending a sweet Eid SMS' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla eid SMS 2018<span>ঈদ এসএমএস ২০১৮</span></h1>
            <p>Eid is all about sharing and caring, let your beloved people know how much you care about them by sending a sweet Eid SMS</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default EidSMS

export const pageQuery = graphql`
  query EidQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
