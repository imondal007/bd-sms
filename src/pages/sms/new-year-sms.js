import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class NewYearSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const newyear = filter(posts, item => item.node.category == 'New Year')
    const orderByLatest = newyear.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>New Year SMS 2019 [নিউ ইয়ার এসএমএস]</title>
          <meta name='description' content='নতুন বছরের শুভেচ্ছা জানাতে ১০০ টি সুন্দর সুন্দর New Year SMS আপনাদের জন্যে। নতুন বছর শুরু হোক সুখ সমৃদ্ধি আর ভালো কিছু পাওয়ার মধ্যে দিয়ে। নিজে ভালো থাকা আর সবাইকে ভালো রাখা হোক এবছরে আমাদের সবার উদ্দেশ্য' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>New Year SMS 2019<span>নিউ ইয়ার এসএমএস ২০১৯</span></h1>
            <p>নতুন বছরের শুভেচ্ছা জানাতে ১০০ টি সুন্দর সুন্দর New Year SMS আপনাদের জন্যে। নতুন বছর শুরু হোক সুখ সমৃদ্ধি আর ভালো কিছু পাওয়ার মধ্যে দিয়ে। নিজে ভালো থাকা আর সবাইকে ভালো রাখা হোক এবছরে আমাদের সবার উদ্দেশ্য</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default NewYearSMS

export const pageQuery = graphql`
  query NewYearQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`