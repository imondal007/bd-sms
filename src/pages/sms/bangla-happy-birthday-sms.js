import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class BirthSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const birth = filter(posts, item => item.node.category == 'Birth Day')
    const orderByLatest = birth.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Happy Birthday SMS 2018 [শুভ জন্মদিন এসএমএস]</title>
          <meta name='description' content='Birthday is a very important day of human life, to make your family and friend feel more special on their birthday send a cute Birthday SMS' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla Happy Birthday SMS 2018<span>শুভ জন্মদিন এসএমএস ২০১৮</span></h1>
            <p>Birthday is a very important day of human life, to make your family and friend feel more special on their birthday send a cute Birthday SMS</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default BirthSMS

export const pageQuery = graphql`
  query BirthQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
