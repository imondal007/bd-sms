import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class SadSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const sad = filter(posts, item => item.node.category == 'Sad')
    const orderByLatest = sad.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Sad SMS 2018 [কস্টের এসএমএস]</title>
          <meta name='description' content='Some time we become sad somewhere in our life, Express your sadness to people who care about you by sending a bold sad SMS' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla Sad SMS 2018<span>কস্টের এসএমএস ২০১৮</span></h1>
            <p>Some time we become sad somewhere in our life, Express your sadness to people who care about you by sending a bold sad SMS</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default SadSMS

export const pageQuery = graphql`
  query SadQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`