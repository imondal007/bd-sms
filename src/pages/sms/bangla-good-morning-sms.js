import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class MorningSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const morning = filter(posts, item => item.node.category == 'Good Morning')
    const orderByLatest = morning.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Good Morning SMS 2018 [শুভ সকাল এসএমএস]</title>
          <meta name='description' content='A lovely good morning sms can bring smile to your favorite one. Send one from our Good morning sms collection now' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla Good Morning SMS 2018<span>শুভ সকাল এসএমএস ২০১৮</span></h1>
            <p>A lovely good morning sms can bring smile to your favorite person. Send one from our Good morning sms collection now</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default MorningSMS

export const pageQuery = graphql`
  query MorningQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
