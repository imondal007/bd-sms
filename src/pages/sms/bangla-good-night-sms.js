import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class NightSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const night = filter(posts, item => item.node.category == 'Good Night')
    const orderByLatest = night.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Good Night SMS 2018 [শুভ রাত্রি এসএমএস]</title>
          <meta name='description' content="Everyone feels extra emotional at night, if you want to stay closer to someone's mind send that person a good night SMS." />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla Good Night SMS 2018<span>শুভ রাত্রি এসএমএস ২০১৮</span></h1>
            <p>Everyone feels extra emotional at night, if you want to stay closer to someone's mind send that person a good night SMS.</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default NightSMS

export const pageQuery = graphql`
  query NightQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
