import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class NoboBorsoSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const noboborso = filter(posts, item => item.node.category == 'Nobo Borso')
    const orderByLatest = noboborso.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Nobo Borso SMS 2018 [বাংলা নববর্ষ এসএমএস]</title>
          <meta name='description' content='বছর ঘুরে এলো পহেলা বৈশাখ, বাঙ্গালী জাতীর হাজার বছরের ঐতিহ্যবাহী বর্ষবরণের দিনটিতে সবাইকে শুভেচ্ছা জানাতে ১০০ টি Bangla Nobo Borso SMS আপনাদের জন্যে' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla Nobo Borso SMS & Pohela Boishakh SMS<span>শুভ নববর্ষ এসএমএস ২০১৮</span></h1>
            <p>বছর ঘুরে এলো পহেলা বৈশাখ, বাঙ্গালী জাতীর হাজার বছরের ঐতিহ্যবাহী বর্ষবরণের দিনটিতে সবাইকে শুভেচ্ছা জানাতে ১০০ টি Bangla Nobo Borso SMS আপনাদের জন্যে</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default NoboBorsoSMS

export const pageQuery = graphql`
  query NoboBorsoQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`