import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import filter from 'lodash/filter'
import Helmet from 'react-helmet'

import PostCard from '../../components/PostCard'
import SideBar from '../../components/SideBar'

class LoveSMS extends React.Component {
  render() {
    const location = get(this, 'props.location.pathname')
    const posts = get(this, 'props.data.allPosts.edges')
    const love = filter(posts, item => item.node.category == 'Love')
    const orderByLatest = love.reverse()

    return (
      <div className='container main-wrap'>
        <Helmet>
          <title>Bangla Romantic Love SMS 2018 [ভালোবাসার রোম্যান্টিক এসএমএস]</title>
          <meta name='description' content='প্রিয়জনকে ভালোবাসার কথা জানিয়ে দিন Romantic Love SMS পাঠিয়ে, রোম্যান্টিক মনের  সব আবেগ অনুভূতি প্রকাশ করতে Romantic Love SMS সেন্ড করুন। Bangla Romantic Love SMS দিয়ে দূর থেকে ছুয়ে দিন ভালোবাসার মানুষের মন' />
          <link rel="canonical" href={`https://bd-sms.com${location}`} />
        </Helmet>
        <article className='article-body'>
          <header className='article-head'>
            <h1>Bangla Romantic Love SMS 2018<span>ভালোবাসার রোম্যান্টিক এসএমএস ২০১৮</span></h1>
            <p>প্রিয়জনকে ভালোবাসার কথা জানিয়ে দিন Romantic Love SMS পাঠিয়ে, রোম্যান্টিক মনের  সব আবেগ অনুভূতি প্রকাশ করতে Romantic Love SMS সেন্ড করুন। Bangla Romantic Love SMS দিয়ে দূর থেকে ছুয়ে দিন ভালোবাসার মানুষের মন</p>
          </header>
          {orderByLatest.map(({ node }) => {
            return (
              <PostCard
                key={node.id}
                text={node.text}
                category={node.category}
                handleCopy={this.handleCopy}
              />
            )
          })}
        </article>
        <SideBar />
      </div>
    )
  }
}

export default LoveSMS

export const pageQuery = graphql`
  query LoveQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
