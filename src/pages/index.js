import React from 'react'
import Link from 'gatsby-link'
import get from 'lodash/get'
import Helmet from 'react-helmet'

import PostCard from '../components/PostCard'
import SideBar from '../components/SideBar'

const BlogIndex = (props) => {
  const siteTitle = get(props, 'data.site.siteMetadata.title')
  const posts = get(props, 'data.allPosts.edges')

  return (
    <div>
      <Helmet>
        <title>New Bangla SMS: BD SMS [বাংলা এসএমএস]</title>
        <meta name='description' content='Largest collection of bangla SMS in the Internet. All latest & regularly updated for 2018' />
        <link rel="canonical" href="https://bd-sms.com" />
      </Helmet>
      <section>
        <div className='container main-wrap'>
          <article className='article-body'>
            <h1>New Bangla SMS collection 2018</h1>
            {posts.map(({ node }) => {
              return (
                <PostCard
                  key={node.id}
                  text={node.text}
                  category={node.category}
                />
              )
            })}
          </article>
          <SideBar />
        </div>
      </section>
    </div>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query IndexQuery {
    allPosts {
      edges {
        node {
          text
          category
          id
        }
      }
    }
  }
`
