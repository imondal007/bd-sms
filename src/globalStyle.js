export const container = {
  maxWidth: 960,
  margin: '0 auto',
  padding: '0 20px'
};

export const d_flex = {
  display: 'flex'
};

export const space_between = {
  justifyContent: 'space-between'
};

export const center = {
  justifyContent: 'center'
};

export const a_center = {
  alignItems: 'center'
};

export const content_spacing = {
  padding: '20px'
}