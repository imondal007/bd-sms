import Rebase from 're-base'
import firebase from 'firebase/app'
import 'firebase/database'

export const app = firebase.initializeApp({
  apiKey: "AIzaSyBh8cVNGpMHtukG_QIJnmbHmoEjV3M1r-g",
  authDomain: "bd-sms.firebaseapp.com",
  databaseURL: "https://bd-sms.firebaseio.com"
});
const db = firebase.database(app);
const base = Rebase.createClass(db);

export default base;
